//
//  AppDelegate.h
//  EstudoGitLab
//
//  Created by Bruno Marçal Lacerda Fonseca on 11/09/17.
//  Copyright © 2017 CI&T. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

